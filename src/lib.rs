/* src/lib.rs
 *
 * Copyright (C) 2015-2016 Nickolay Ilyushin <nickolay02@inbox.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#[macro_export(he_mk)]
macro_rules! he_mk {
    ( init, $html:ident ) => ( let $html = "<!DOCTYPE html>\n<html>\n" );
    ( head, $html:ident ) => ( let $html = $html.to_string() + "<head>\n" );
    ( head_title, $html:ident, title => $title:expr ) => ( let $html = $html.to_string() + &format!("<title>{}</title>\n", $title));
    ( body, $html:ident ) => ( let $html = $html.to_string() + &"</head>\n<body>\n");
    ( end, $html:ident ) => ( let $html = $html.to_string() + &"</body>\n</html>");

    ( p, $html:ident, text => $text:expr ) => ( let $html = $html.to_string() + &format!("<p>{}</p>\n", $text));
    ( h1, $html:ident, text => $text:expr ) => ( let $html = $html.to_string() + &format!("<h1>{}</h1>\n", $text));
    ( h2, $html:ident, text => $text:expr ) => ( let $html = $html.to_string() + &format!("<h2>{}</h2>\n", $text));
    ( h3, $html:ident, text => $text:expr ) => ( let $html = $html.to_string() + &format!("<h3>{}</h3>\n", $text));
    ( h4, $html:ident, text => $text:expr ) => ( let $html = $html.to_string() + &format!("<h4>{}</h4>\n", $text));
    ( h5, $html:ident, text => $text:expr ) => ( let $html = $html.to_string() + &format!("<h5>{}</h5>\n", $text));
    ( h6, $html:ident, text => $text:expr ) => ( let $html = $html.to_string() + &format!("<h6>{}</h6>\n", $text));

    ( p, $html:ident, text => $text:expr, align => $align:expr ) => ( let $html = $html.to_string() + &format!("<p align='{}'>{}</p>\n", $align, $text));
    ( h1, $html:ident, text => $text:expr, align => $align:expr ) => ( let $html = $html.to_string() + &format!("<h1 align='{}'>{}</h1>\n", $align, $text));
    ( h2, $html:ident, text => $text:expr, align => $align:expr ) => ( let $html = $html.to_string() + &format!("<h2 align='{}'>{}</h2>\n", $align, $text));
    ( h3, $html:ident, text => $text:expr, align => $align:expr ) => ( let $html = $html.to_string() + &format!("<h3 align='{}'>{}</h3>\n", $align, $text));
    ( h4, $html:ident, text => $text:expr, align => $align:expr ) => ( let $html = $html.to_string() + &format!("<h4 align='{}'>{}</h4>\n", $align, $text));
    ( h5, $html:ident, text => $text:expr, align => $align:expr ) => ( let $html = $html.to_string() + &format!("<h5 align='{}'>{}</h5>\n", $align, $text));
    ( h6, $html:ident, text => $text:expr, align => $align:expr ) => ( let $html = $html.to_string() + &format!("<h6 align='{}'>{}</h6>\n", $align, $text));

    ( br, $html:ident ) => ( let $html = $html.to_string() + "<br/>\n");
    ( hr, $html:ident ) => ( let $html = $html.to_string() + "<hr/>\n");
}
